package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int rows;
	int cols;
	
	CellState[][] grid;
	

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		this.grid = new CellState[rows][columns];
		for (int row = 0; row < rows; row++) {
			for (int col=0; col < columns; col++) {
				set(row, col, initialState);
			}
		}
		
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }
    
  
    @Override
    public void set(int row, int column, CellState element) {
    	grid[row][column] = element;
		
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    
    @Override
    public IGrid copy() {
        CellGrid copy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < numRows(); i++) {
        	for (int j = 0; j < numColumns(); j++) {
        		copy.set(i, j, this.grid[i][j]);
        	}
        }
        return copy;
    }
    
}
